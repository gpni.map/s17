// console.log("Hello!");



/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function getDetails(){
		let fullName = prompt("Please enter your full name. ");
		let age = prompt("Please enter your age.");
		let location = prompt("Please enter your location");
		
		alert("Thank you for your input.");
		
		console.log("Hello " + fullName + "!");
		console.log("You are " + age + " years old.");
		console.log("You are from " + location + ".");
	};

	getDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function displayArtist(){

		let artists = ["Lawrence Ward", "Parokya Ni Edgar", "Pentatonix", "Maroon 5", "Jason Mraz"];

		console.log ("Top 5 Favorite Bands/Musical Artist:");
		console.log	("1." + artists[0]);
		console.log	("2." + artists[1]);
		console.log	("3." + artists[2]);
		console.log	("4." + artists[3]);
		console.log	("5." + artists[4]);

	};

	displayArtist();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function listMovies(){
		let movie1 = "Avengers: End Game";
		let ratingMovie1 = 94;
		
		let movie2 = "IP Man (2008)";
		let ratingMovie2 = 86;

		let movie3 = "The Wolverine (2013)";
		let ratingMovie3 = 71;

		let movie4 = "How to Train You Dragon: The Hidden World";
		let ratingMovie4 = 90;

		let movie5 = "Jumanji: Welcome to the Jungle";
		let ratingMovie5 = 76;



		console.log("Top 5 Favorite Movies:");
		console.log("1." + movie1);
		console.log("Rotten Tomatoes Rating: " + ratingMovie1 + "%");

		console.log("2." + movie2);
		console.log("Rotten Tomatoes Rating: " + ratingMovie2 + "%");

		console.log("3." + movie3);
		console.log("Rotten Tomatoes Rating: " + ratingMovie3 + "%");

		console.log("4." + movie4);
		console.log("Rotten Tomatoes Rating: " + ratingMovie4 + "%");

		console.log("5." + movie5);
		console.log("Rotten Tomatoes Rating: " + ratingMovie5 + "%");

	}

	listMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/



let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

