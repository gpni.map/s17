//JAVA SCRIPT FUNCTION Discussion//


/*

	functions
		- functions are lines/ block of codes that tells our device/application
		to perform certain task when called/invoked.
		- functions are mostly created to create complicated task to run several lines of codes in succession.
		- also used to prevent repeating lines/block of codes that perform the same function

	syntax:

		function functionName (){
			code block(statement);
		}		

	>> function keyword - used to define a javascript functions
	>> function name - functions are named uniquely to able to use later in the code
	>> function block (){} - the statement which comprise the body of the function. This is where the code is placed to be executed.

*/

function printName(){
	console.log('My name is Glenn. I am a FSWD');
}


// function invocation - it is a common term used to call a function.

printName();

declaredFunction ();
// result error because it is not yet defined

function declaredFunction(){
	console.log('This is a defined function');
}




/*
	Function Declaration VS Expression

		A function can be created through function declaration by using the function keyword
		and adding a function name.

		Declared functions are not executed immediately. They are "save for late use", and will be executed later
		when they are invoked.

*/

function declaredFunction2(){
	console.log("Hi I am form declared function().");
};


declaredFunction2(); // declared functiond can be hoisted as long as the function has been defined.
declaredFunction2();
declaredFunction2();


/*
	Function Expression
		- a function can also be stored in a variable.
		- a function expression is an anonymous function since it is assigned to a variable

		anonymous function is a function without a name.

*/

let variableFunction = function(){
	console.log("I am from variable function.");
};

variableFunction();


/*
	We can also create a function expression of a named function. However, 
	to invoke it, we should invoke it by its variable name and not by its function name.

	
	Function expressions are always invoked using the variable name.

*/


let funcExpression = function funcName(){
	console.log("Hello from the other side.");
};

funcExpression();



/*
	You can reassign declared function and functin expression to new anonymous function
*/


declaredFunction = function (){
	console.log('Updated declared function');
};

declaredFunction();


funcExpression = function(){
	console.log('Updated function expression.');
};

funcExpression();


const constantFunction = function(){
	console.log('Initialized with const.');
};

constantFunction();


// constantFunction = function(){
// 	console.log('Cannot be reassigned');
// };

// constantFunction();
//reassignment with const function expression is not possible.



/*
	FUNCTION SCOPING

	Scope is the accessibility (visibility) of variables within our program.
	 - JavaScript Variables has 3 types of scope:
	  1. Local/block scope
	  2. Global scope
	  3. function scope

*/


{
	let localVar = "Kim Seok-jin";
}

let globalVar = "The World's most handsome.";

// console.log(localVar);

//a local variable will be visible only within a function where it is defined.


console.log(globalVar);

//a global variable has global scope which means it can be defined anywhere in you JS code


//FUNCTION SCOPE

/*
	JavaScript has a function scope.
	Each function creates a new scope.
	Variables defined inside a function are not accessible (visible)
	outside the function

	Variable declared with var, let and const are quite similar when declared inside a function.
*/

function showNames(){
	var functionVar = 'Jungkook';
	const functionConst = "BTS";
	let functionLet = 'Kookie';

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);

}

showNames();


// NESTED FUNCTION
/*
	You can create another function inside a function. This is called a nested function. This nested function, being inside a new function will have access in variables or names as they are within the same scope/code block.
*/


function myNewFunction(){
	let name = "Yor";

	function nestedFunction(){
		let nestedName = 'Brando';
		console.log(nestedName);
	};
	//console.log(nestedName); // result: err, nestedName for function scope
	nestedFunction();
};

myNewFunction();


//Function and Global Scoped Variables
// Global Scoped Variable

let globalName = "Glenn";

function myNewFunction2(){

	let nameInside = 'Kim';
	//Variable declared globally (outside function) have global scope feature
	//Global variables can be access from anywhere in a JavaScript program including form inside a function.
	console.log(globalName);
	console.log(nameInside);
};

myNewFunction2();



//Alert Function

/*
	alert()

	Syntax: 
		alert('message');
*/

alert("Hello World!");


function showSampleAlert(){
	alert("Hello user!");
};

showSampleAlert();

console.log('I will only log in the console when the alert is dismissed.');


//PROMPT FUNCTION

/*
	Syntax:
		prompt('dialog');
*/


let samplePrompt = prompt("Enter your Name: ");
console.log("Hello " + samplePrompt);


let sampleNullPrompt = prompt('Dont input anything');
console.log(sampleNullPrompt);
//if prompt is cancelled, the result will be: null
// if there is no input in the prompt, the result will be: empty string or blank.


function printWelcomeMessage(){
	let firstName = prompt("Enter your first name: ");
	let lastName = prompt("Enter your last name: ");

	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to Gamer's Guild.");
};

printWelcomeMessage();


//Function Naming Convention
/*
	- it should be definitive of its task. Usually contains a verb.
*/


function getCourses(){
	let courses = ['Programming 100','Science 101','Grammar 102','Mathematics 103'];
	console.log(courses);

};

getCourses();


//AVOID GENERIC NAMES to AVOID CONFUSION!
function get(){
	let name = "Jimmin";
	console.log(name);
};


//AVOID POINTLESS AND INAPPROPRIATE FUNCTION NAMES
function foo(){
	console.log(25%25);
};

foo();


//name function in smallcaps. FOllow camelCase when naming function


function displayCarInfo(){
	console.log("brand: Toyota");
	console.log("type: Sedan");
	console.log("Price: 1,500,000");
};

displayCarInfo();